class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.integer :amount
      t.string :method
      t.string :status
      t.references :buyer, foreign_key: true
      t.references :client, foreign_key: true

      t.timestamps
    end
  end
end
