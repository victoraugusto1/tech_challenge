class CreateBankSlips < ActiveRecord::Migration[5.2]
  def change
    create_table :bank_slips do |t|
      t.string :number
      t.references :payment, foreign_key: true

      t.timestamps
    end
  end
end
