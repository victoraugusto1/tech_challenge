FROM ruby:2.5.3

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /payment_api

WORKDIR /payment_api

COPY Gemfile /payment_api/Gemfile
COPY Gemfile.lock /payment_api/Gemfile.lock

RUN bundle install

COPY . /payment_api
