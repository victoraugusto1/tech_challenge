class Payment
  class PaymentCreator
    def initialize(client_params:, buyer_params:, payment_params:)
      @client = client_params
      @buyer = buyer_params
      @payment = payment_params
    end

    def create
      ActiveRecord::Base.transaction do
        client = Client.find_or_create_by(id: @client[:id])
        client.save! if client.new_record?
        buyer = find_or_create_buyer
        buyer.save! if buyer.new_record?
        payment = create_payment(buyer, client)
        payment_method = create_method_for_payment(payment)
        payment.save! && payment_method.save!
        payment
      end
    end

    private

    def create_payment(buyer, client)
      payment_status = set_payment_status
      payment = buyer.payments.new(amount: @payment[:amount], method: @payment[:method], status: payment_status)
      payment.client = client
      payment
    end

    def create_method_for_payment(payment)
      if payment.method == 'credit_card'
        payment.build_credit_card(@payment[:credit_card])
      elsif payment.method == 'bank_slip'
        payment.build_bank_slip
      end
    end

    def set_payment_status
      if @payment[:method] == 'credit_card'
        %w[approved declined].sample
      elsif @payment[:method] == 'bank_slip'
        'analyzing'
      end
    end

    def find_or_create_buyer
      Buyer.find_or_create_by(email: @buyer[:email]) do |buyer|
        buyer.name = @buyer[:name]
        buyer.taxpayer_registry = @buyer[:taxpayer_registry]
      end
    end
  end
end
