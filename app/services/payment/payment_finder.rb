class Payment
  class PaymentFinder
    def initialize(param)
      @payment_id = param[:payment_id]
    end

    def find_payment
      payment = Payment.find_by_id(@payment_id)
      return if payment.nil?

      buyer = payment.buyer
      client = payment.client
      payment_method = payment.bank_slip || payment.credit_card
      { client: client, buyer: buyer, payment: payment, payment.method => payment_method }
    end
  end
end
