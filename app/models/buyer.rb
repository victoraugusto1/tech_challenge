class Buyer < ApplicationRecord
  before_validation :normalize_taxpayer_registry

  has_many :payments

  validates_presence_of :name, :email, :taxpayer_registry
  validates_length_of :taxpayer_registry, is: 11

  def normalize_taxpayer_registry
    taxpayer_registry.delete! ',.-' if taxpayer_registry.present?
  end
end
