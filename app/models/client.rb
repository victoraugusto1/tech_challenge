class Client < ApplicationRecord
  has_many :payments
  has_many :buyers, through: :payments
end
