class CreditCard < ApplicationRecord
  belongs_to :payment

  validates_presence_of :cardholder_name, :number, :expiration_date, :cvv
end
