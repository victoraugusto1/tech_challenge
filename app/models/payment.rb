class Payment < ApplicationRecord
  belongs_to :buyer
  belongs_to :client

  has_one :bank_slip
  has_one :credit_card

  validates_presence_of :amount, :method
  validates_inclusion_of :method, in: %w[credit_card bank_slip], message: '%{value} is not a valid payment method.'
  validates_inclusion_of :status, in: %w[approved declined analyzing], message: '%{value} is not a valid payment status.'
end
