class BankSlip < ApplicationRecord
  after_initialize :set_bank_slip_number

  validates :number, uniqueness: true

  belongs_to :payment

  private

  def set_bank_slip_number
    self.number = generate_bank_slip_number
  end

  def generate_bank_slip_number
    number = rand(1..9).to_s
    loop do
      46.times do
        number << rand(0..9).to_s
      end
      break unless BankSlip.where(number: number).exists?
    end
    number
  end
end
