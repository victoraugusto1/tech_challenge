module Api
  module V1
    class PaymentsController < ApplicationController
      rescue_from ActionController::ParameterMissing do |exception|
        render json: exception, status: :bad_request
      end

      def create
        payment = Payment::PaymentCreator.new(
          client_params: payment_creator_params[:client],
          buyer_params: payment_creator_params[:buyer],
          payment_params: payment_creator_params[:payment]
        ).create
        if payment.credit_card.present?
          render json: { payment_id: payment.id, payment_status: payment.status }, status: 201
        elsif payment.bank_slip.present?
          render json: { payment_id: payment.id, bank_slip_number: payment.bank_slip.number }, status: 201
        end
      rescue StandardError => e
        render json: { error: e }, status: 500
      end

      def show
        payment = Payment::PaymentFinder.new(payment_finder_params).find_payment
        if payment
          render json: payment.as_json(except: %w[created_at updated_at buyer_id client_id payment_id])
        else
          head :no_content
        end
      end

      private

      def payment_creator_params
        params.require(:client)
        params.require(:buyer)
        params.require(:payment)
        params.permit(
          client: [:id],
          buyer: %i[name email taxpayer_registry],
          payment: [:amount,
                    :method,
                    credit_card: %i[cardholder_name number expiration_date cvv]]
        )
      end

      def payment_finder_params
        params.require(:payment_id)
        params.permit(:payment_id)
      end
    end
  end
end
