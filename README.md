# Payment API

## Introduction

The Payment API is a sample project that creates and consults mocked payments.

## Setup

### Using Docker
Install Docker if you haven't: https://docs.docker.com/install/

After running Docker, open your terminal in the project root folder and run `docker-compose up`. This will pull the necessary images, build and start the containers. After doing that, you can confirm if the application and the database containers are up by running the `docker ps` command.

To run unit tests, execute the following command on the project folder:

`docker-compose run app rspec`

### Without using Docker
This project was built using the Ruby 2.5.3 version. You can download it through the ruby-lang website: https://www.ruby-lang.org/en/documentation/installation/

The database chosen was PostgreSQL, and you can download it here: https://www.postgresql.org/download/

With Ruby and PostgreSQL installed, open your terminal on the project folder and run `bundle install` to install the dependencies. Then, on the `database.yml` file, comment the host parameter:

```ruby
  encoding: unicode
# host: db
  username: postgres
```

After that, just run `rails s` and the server will be up. 

To run the unit tests, run the `rspec` command on the project folder.

## Sample requests and responses
### To create payments (`POST /api/v1/payments`)
#### Credit card payments

###### JSON request:

```
{
  "client": {
    "id": "1"
  },
  "buyer": {
    "name": "John Smith",
    "email": "john@smith.com",
    "taxpayer_registry": "123.456.789-00"
  },
  "payment": {
    "amount": "10000",
    "method": "credit_card",
    "credit_card": {
      "cardholder_name": "John Smith",
      "number": "5555666677778884",
      "expiration_date": "12/10/2022",
      "cvv": "123"
    }
  }
}
```

###### Answer:

```
{
  "payment_id": 1,
  "payment_status": "declined"
}
```

#### Bank slip payments

###### JSON request:

```
{
  "client": {
    "id": "1"
  },
  "buyer": {
    "name": "John Smith",
    "email": "john@smith.com",
    "taxpayer_registry": "98765432100"
  },
  "payment": {
    "amount": "10000",
    "method": "bank_slip"
  }
}
```

###### Answer:

```
{
  "payment_id": 18,
  "bank_slip_number": "47443460225117430197264256862951173647230894637"
}
```

### To consult payments (`GET /api/v1/payments`)

#### Credit card payments

###### JSON request

```
{
  "payment_id": "2"
}
```

###### Answer

```
{
  "client": {
    "id": 1
  },
  "buyer": {
    "id": 1,
    "name": "John Smith",
    "email": "john@smith.com",
    "taxpayer_registry": "98765432100"
  },
  "payment": {
    "id": 2,
    "amount": 10000,
    "method": "credit_card",
    "status": "approved"
  },
  "credit_card": {
    "id": 1,
    "cardholder_name": "John Smith",
    "number": "5555666677778884",
    "expiration_date": "12/10/2022",
    "cvv": "123"
  }
}
```

#### Bank slip payments

###### JSON request

```
{
  "payment_id": "1"
}
```

###### Answer

```
{
  "client": {
    "id": 1
  },
  "buyer": {
    "id": 1,
    "name": "John Smith",
    "email": "john@smith.com",
    "taxpayer_registry": "98765432100"
  },
  "payment": {
    "id": 1,
    "amount": 10000,
    "method": "bank_slip",
    "status": "analyzing"
  },
  "bank_slip": {
    "id": 1,
    "number": "23680868110297026608991467770278122884375457862"
  }
}
```

## About the project architecture and design
The service object design pattern was chosen to create and consult payments. The `PaymentCreator` class is responsible to create and save payments, buyers, clients and payment methods only if they are all valid. It also assigns a status to the payment. The `PaymentFinder` is responsible to find payments, buyers, clients and payment methods that are associated and construct the JSON response that will be sent by the controller.

The `PaymentsController` is only responsible for taking care of the requests and answers. It calls the payments services, process the output and send it as an answer, with a status code.

The models represents the entities involved: buyers, clients, payments, credit cards and bank slips. Each one have its own validations.
