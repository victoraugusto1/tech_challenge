Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      match 'payments', to: 'payments#create', via: [:post]
      match 'payments', to: 'payments#show', via: [:get]
    end
  end
end
