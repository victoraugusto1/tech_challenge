require 'rails_helper'

RSpec.describe 'Payments', type: :request do
  let(:client_and_buyer_params) do
    {
      client: {
        id: '1'
      },
      buyer: {
        name: Faker::Name.name,
        email: Faker::Internet.email,
        taxpayer_registry: Faker::CPF.pretty
      }
    }
  end

  let(:credit_card_payment_params) do
    {
      payment: {
        amount: 100_00,
        method: 'credit_card',
        credit_card: {
          cardholder_name: Faker::Name.name,
          number: Faker::Finance.credit_card,
          expiration_date: '10/10/2025',
          cvv: '123'
        }
      }
    }
  end

  let(:bank_slip_payment_params) do
    {
      payment: {
        amount: 100_00,
        method: 'bank_slip'
      }
    }
  end

  describe 'POST /payments' do
    context 'valid data' do
      context 'with credit card as payment method' do
        before { post '/api/v1/payments', params: credit_card_payment_params.merge(client_and_buyer_params) }

        it 'responds with 201' do
          expect(response).to have_http_status(201)
        end

        it 'returns payment id and status' do
          expect(response.body).to include('payment_status')
        end
      end

      context 'with bank slip as payment method' do
        before { post '/api/v1/payments', params: bank_slip_payment_params.merge(client_and_buyer_params) }

        it 'responds with 201' do
          expect(response).to have_http_status(201)
        end

        it 'returns payment id and bank slip number' do
          expect(response.body).to include('bank_slip_number')
        end
      end
    end

    context 'invalid data' do
      it 'responds with 422' do
        post '/api/v1/payments', params: client_and_buyer_params
        expect(response).to have_http_status(500)
      end
    end
  end

  describe 'GET /payments' do
    it 'responds with 404' do
      get '/api/v1/payments', params: { payment_id: '1' }
      expect(response).to have_http_status(204)
    end

    it 'returns the payment information' do
      post '/api/v1/payments', params: bank_slip_payment_params.merge(client_and_buyer_params)
      get '/api/v1/payments', params: { payment_id: JSON[response.body]['payment_id'] }
      expect(response).to have_http_status(200)
      expect(response.body).to include('name', 'buyer', 'payment', 'status')
    end
  end
end
