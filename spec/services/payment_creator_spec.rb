require 'rails_helper'

RSpec.describe Payment::PaymentCreator do
  describe '#create' do
    let(:first_client_params) do
      { id: '1' }
    end

    let(:first_buyer_params) do
      {
        name: Faker::Name.name,
        email: Faker::Internet.email,
        taxpayer_registry: Faker::CPF.pretty
      }
    end

    let(:credit_card_payment_params) do
      {
        amount: 100_00,
        method: 'credit_card',
        credit_card: {
          cardholder_name: Faker::Name.name,
          number: Faker::Finance.credit_card,
          expiration_date: '10/10/2025',
          cvv: '123'
        }
      }
    end

    let(:bank_slip_payment_params) do
      {
        amount: 100_00,
        method: 'bank_slip'
      }
    end

    subject do
      described_class.new(
        client_params: first_client_params,
        buyer_params: first_buyer_params,
        payment_params: credit_card_payment_params
      )
    end

    context 'when is a new client' do
      context 'when is a new buyer' do
        it 'changes the client counts' do
          expect { subject.create }.to change { Client.count }.from(0).to(1)
        end

        it 'changes the client counts' do
          expect { subject.create }.to change { Buyer.count }.from(0).to(1)
        end

        it 'creates credit card payment' do
          expect { subject.create }.to change { CreditCard.count }.from(0).to(1)
        end

        it 'creates bank slip payment' do
          expect {
            described_class.new(
              client_params: first_client_params,
              buyer_params: first_buyer_params,
              payment_params: bank_slip_payment_params
            ).create
          }.to change { BankSlip.count }.from(0).to(1)
        end
      end

      context 'when is an existing buyer' do
        let(:second_client_params) do
          { id: '2' }
        end

        before do
          described_class.new(
            client_params: second_client_params,
            buyer_params: first_buyer_params,
            payment_params: credit_card_payment_params
          ).create
        end

        it 'changes the client count' do
          expect { subject.create }.to change { Client.count }.from(1).to(2)
        end

        it 'does not change the buyer count' do
          expect { subject.create }.not_to(change { Buyer.count })
        end

        it 'creates credit card payment' do
          expect { subject.create }.to change { CreditCard.count }.from(1).to(2)
        end

        it 'creates bank slip payment' do
          expect {
            described_class.new(
              client_params: first_client_params,
              buyer_params: first_buyer_params,
              payment_params: bank_slip_payment_params
            ).create
          }.to change { BankSlip.count }.from(0).to(1)
        end
      end
    end

    context 'when is an existing client' do
      let(:second_buyer_params) do
        {
          name: Faker::Name.name_with_middle,
          email: Faker::Internet.free_email,
          taxpayer_registry: Faker::CPF.numeric
        }
      end

      context 'when is a new buyer' do
        before do
          described_class.new(
            client_params: first_client_params,
            buyer_params: second_buyer_params,
            payment_params: credit_card_payment_params
          ).create
        end

        it 'changes the buyer counts' do
          expect { subject.create }.to change { Buyer.count }.from(1).to(2)
        end
        it 'does not change the client counts' do
          expect { subject.create }.not_to(change { Client.count })
        end

        it 'creates credit card payment' do
          expect { subject.create }.to change { CreditCard.count }.from(1).to(2)
        end

        it 'creates bank slip payment' do
          expect {
            described_class.new(
              client_params: first_client_params,
              buyer_params: first_buyer_params,
              payment_params: bank_slip_payment_params
            ).create
          }.to change { BankSlip.count }.from(0).to(1)
        end
      end

      context 'when is an existing buyer' do
        before do
          subject.create
        end

        it 'does not change the client count' do
          expect { subject.create }.not_to(change { Client.count })
        end

        it 'does not change the buyer count' do
          expect { subject.create }.not_to(change { Buyer.count })
        end

        it 'creates credit card payment' do
          expect { subject.create }.to(change { CreditCard.count }.from(1).to(2))
        end

        it 'creates bank slip payment' do
          expect {
            described_class.new(
              client_params: first_client_params,
              buyer_params: first_buyer_params,
              payment_params: bank_slip_payment_params
            ).create
          }.to change { BankSlip.count }.from(0).to(1)
        end
      end
    end
  end
end
