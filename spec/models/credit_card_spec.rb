require 'rails_helper'

RSpec.describe CreditCard, type: :model do
  context 'relationships' do
    it { is_expected.to belong_to(:payment) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:cardholder_name) }
    it { is_expected.to validate_presence_of(:number) }
    it { is_expected.to validate_presence_of(:expiration_date) }
    it { is_expected.to validate_presence_of(:cvv) }
  end
end
