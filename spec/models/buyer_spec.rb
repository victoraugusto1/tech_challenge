require 'rails_helper'

RSpec.describe Buyer, type: :model do
  context 'relationships' do
    it { is_expected.to have_many(:payments) }
  end

  context 'validations' do
    context 'presence' do
      it { is_expected.to validate_presence_of(:name) }
      it { is_expected.to validate_presence_of(:email) }
      it { is_expected.to validate_presence_of(:taxpayer_registry) }
    end

    context 'length' do
      it { is_expected.to validate_length_of(:taxpayer_registry).is_equal_to(11) }
    end
  end

  context 'callbacks' do
    it '#normalize_taxpayer_registry' do
      pretty_taxpayer_registry = Faker::CPF.pretty
      subject.taxpayer_registry = pretty_taxpayer_registry
      expect { subject.valid? }
        .to change { subject.taxpayer_registry }
        .from(pretty_taxpayer_registry)
        .to(pretty_taxpayer_registry.delete('^0-9'))
    end
  end
end
