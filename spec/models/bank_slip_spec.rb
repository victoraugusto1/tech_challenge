require 'rails_helper'

RSpec.describe BankSlip, type: :model do
  context 'relationships' do
    it { is_expected.to belong_to(:payment) }
  end

  context 'validations' do
    it { is_expected.to validate_uniqueness_of(:number) }
  end

  context 'callbacks' do
    describe '#set_bank_slip_number' do
      it 'fills bank slip number' do
        expect(subject.number).not_to be_empty
      end
    end

    describe '#generate_bank_slip_number' do
      it 'generates number with correct amount of digits' do
        expect(subject.number.length).to equal(47)
      end
    end
  end
end
