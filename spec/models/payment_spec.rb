require 'rails_helper'

RSpec.describe Payment, type: :model do
  context 'relationships' do
    it { is_expected.to belong_to(:buyer) }
    it { is_expected.to belong_to(:client) }
    it { is_expected.to have_one(:credit_card) }
    it { is_expected.to have_one(:bank_slip) }
  end

  context 'validations' do
    context 'presence' do
      it { is_expected.to validate_presence_of(:amount) }
      it { is_expected.to validate_presence_of(:method) }
    end

    context 'inclusion' do
      it { is_expected.to validate_inclusion_of(:method).in_array(%w[credit_card bank_slip]) }
      it { is_expected.to validate_inclusion_of(:status).in_array(%w[approved declined analyzing]) }
    end
  end
end
